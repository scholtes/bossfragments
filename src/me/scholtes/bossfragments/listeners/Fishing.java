package me.scholtes.bossfragments.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;

import me.scholtes.bossfragments.BossFragments;
import me.scholtes.bossfragments.Utils;

public class Fishing implements Listener {
	
	BossFragments plugin;
	Utils utils;
	
	public Fishing(BossFragments plugin, Utils utils) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@EventHandler
	public void onFish(PlayerFishEvent e) {
		if (e.getState() != State.CAUGHT_FISH) return;
		utils.giveFragmentNaturally(e.getPlayer(), "fishing");
		
	}

}
