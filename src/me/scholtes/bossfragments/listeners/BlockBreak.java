package me.scholtes.bossfragments.listeners;

import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.scholtes.bossfragments.BossFragments;
import me.scholtes.bossfragments.Utils;

public class BlockBreak implements Listener {
	
	BossFragments plugin;
	Utils utils;
	
	public BlockBreak(BossFragments plugin, Utils utils) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		
		List<String> blockList = plugin.getConfig().getStringList("bosses.mining.fragment.block_list");
		if (!blockList.contains(e.getBlock().getType().toString())) {
			return;
		}

		utils.giveFragmentNaturally(e.getPlayer(), "fishing");
		
	}

}
