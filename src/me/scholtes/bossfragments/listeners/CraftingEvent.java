package me.scholtes.bossfragments.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.scholtes.bossfragments.BossFragments;
import me.scholtes.bossfragments.Utils;

public class CraftingEvent implements Listener {
	
	BossFragments plugin;
	Utils utils;
	
	public CraftingEvent(BossFragments plugin, Utils utils) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		
		String type = "none";
		if (e.getRecipe().getResult().equals(utils.getBossRecipe("fishing").getResult())) type = "fishing";
		if (e.getRecipe().getResult().equals(utils.getBossRecipe("mining").getResult())) type = "mining";
		if (e.getRecipe().getResult().equals(utils.getBossRecipe("farming").getResult())) type = "farming";
		if (type.equals("none")) return;
		
		String fragmentName = utils.color(plugin.getConfig().getString("bosses." + type + ".fragment.name"));
		List<String> fragmentLore = new ArrayList<String>();
		for (String string : plugin.getConfig().getStringList("bosses." + type + ".fragment.lore")) fragmentLore.add(utils.color(string));
		for (int i = 1; i < 10; i++) {
			ItemMeta meta = e.getInventory().getItem(i).getItemMeta();
			if (meta.getDisplayName() != null && meta.getLore() != null && meta.getDisplayName().equals(fragmentName) && meta.getLore().equals(fragmentLore)) continue;
			e.getInventory().setResult(new ItemStack(Material.AIR));
			return;
		}
		return;
		
	}

}
