package me.scholtes.bossfragments.listeners;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import me.scholtes.bossfragments.BossFragments;
import me.scholtes.bossfragments.Utils;

public class BlockPlace implements Listener {
	
	BossFragments plugin;
	Utils utils;
	Random rand = new Random();
	
	public BlockPlace(BossFragments plugin, Utils utils) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		
		Player p = e.getPlayer();
		ItemStack hand = e.getItemInHand();
		String type = "none";
		if (hand.isSimilar(utils.getItem("fishing", "lootchest"))) type = "fishing";
		if (hand.isSimilar(utils.getItem("mining", "lootchest"))) type = "mining";
		if (hand.isSimilar(utils.getItem("farming", "lootchest"))) type = "farming";
		if (type.equals("none")) return;
		
		e.setCancelled(true);
		
		if (hand.getAmount() > 1) hand.setAmount(hand.getAmount() - 1);
		else p.getInventory().remove(hand);
		
		List<String> rewards = plugin.getConfig().getStringList("bosses." + type + ".lootchest.reward_cmds");
		Collections.shuffle(rewards);
		int max = plugin.getConfig().getInt("bosses." + type + ".lootchest.max_rewards");
		int min = plugin.getConfig().getInt("bosses." + type + ".lootchest.min_rewards");
		int random = rand.nextInt((max - min) + 1) + min;
		for (int i = 0; i < random; i++) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), rewards.get(i).replace("%player%", p.getName().toString()));
		}
		if (!plugin.getConfig().getString("bosses." + type + ".lootchest.sound").equals("none")) {
			p.playSound(p.getLocation(), Sound.valueOf(plugin.getConfig().getString("bosses." + type + ".lootchest.sound")), 1, 1);
		}
		if (!plugin.getConfig().getString("bosses." + type + ".lootchest.open_message").equals("none")) {
			p.sendMessage(utils.color(plugin.getConfig().getString("bosses." + type + ".lootchest.open_message")));
		}
		
	}

}
