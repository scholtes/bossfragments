package me.scholtes.bossfragments;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {
	
	BossFragments plugin;
	
	public Utils(BossFragments plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public ShapedRecipe getBossRecipe(String type) {
		if (type == null) return null;
		if (!type.equals("fishing") && !type.equals("farming") && !type.equals("mining")) return null;
		
		ItemStack spawnEgg = new ItemStack(Material.valueOf(plugin.getConfig().getString("bosses." + type + ".spawnegg.material")), 1);
		ItemMeta meta = spawnEgg.getItemMeta();
		meta.setDisplayName(color(plugin.getConfig().getString("bosses." + type + ".spawnegg.name")));
		List<String> lore = new ArrayList<String>();
		for (String string : plugin.getConfig().getStringList("bosses." + type + ".spawnegg.lore")) lore.add(color(string));
		meta.setLore(lore);
		spawnEgg.setItemMeta(meta);
		
		ShapedRecipe recipe = new ShapedRecipe(spawnEgg);
		recipe.shape("***","***","***");
		recipe.setIngredient('*', Material.valueOf(plugin.getConfig().getString("bosses." + type + ".fragment.material")));
		
		return recipe;
	}
	
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
	
	public ItemStack getItem(String type, String itemType) {
		if (type == null || itemType == null) return null;
		if (!type.equals("fishing") && !type.equals("farming") && !type.equals("mining")) return null;
		
		ItemStack item = new ItemStack(Material.valueOf(plugin.getConfig().getString("bosses." + type + "." + itemType + ".material")), 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(color(plugin.getConfig().getString("bosses." + type + "." + itemType + ".name")));
		List<String> lore = new ArrayList<String>();
		for (String string : plugin.getConfig().getStringList("bosses." + type + "." + itemType + ".lore")) lore.add(color(string));
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		return item;
	}
	
	public void giveFragmentNaturally(Player p, String type) {
		if (type == null) return;
		double randomChance = Math.random() * 100;
		if (randomChance < plugin.getConfig().getDouble("bosses." + type + ".fragment.find_chance")) {
			ItemStack fragment = getItem(type, "fragment");
			if (!plugin.getConfig().getString("bosses." + type + ".fragment.find_sound").equals("none")) {
				p.playSound(p.getLocation(), Sound.valueOf(plugin.getConfig().getString("bosses." + type + ".fragment.find_sound")), 1, 1);
			}
			if (!plugin.getConfig().getString("bosses." + type + ".fragment.find_message").equals("none")) {
				p.sendMessage(color(plugin.getConfig().getString("bosses." + type + ".fragment.find_message")));
			}
			
			if (p.getInventory().containsAtLeast(fragment, 1) || p.getInventory().firstEmpty() != -1) {
				p.getInventory().addItem(fragment);
			}
			else {
				if (!plugin.getConfig().getString("bosses." + type + ".fragment.inventory_full").equals("none")) {
					p.sendMessage(color(plugin.getConfig().getString("bosses." + type + ".fragment.inventory_full")));
				}
				p.getWorld().dropItem(p.getLocation(), fragment);
			}
			
		}
	}
	
}
