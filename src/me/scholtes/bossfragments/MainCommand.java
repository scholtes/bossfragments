package me.scholtes.bossfragments;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCommand implements CommandExecutor {

	BossFragments plugin;
	Utils utils;
	
	public MainCommand(BossFragments plugin, Utils utils) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("bossfragments.admin")) {
			if (!plugin.getConfig().getString("no_perms").equals("none")) {
				sender.sendMessage(utils.color(plugin.getConfig().getString("no_perms")));
			}
			return true;
		}
		if (args.length == 0) helpMessage(sender);
		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				sender.sendMessage(utils.color("&aBossFragments config reloaded!"));
				return true;
			}
			if (args[0].equals("give")) {
				if (args.length == 1 || args.length == 2 || args.length == 3 || args.length == 4) {
					sender.sendMessage(utils.color("&7Write in the arguments!"));
					sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
					return true;
				}
				if (Bukkit.getPlayer(args[1]) == null) {
					sender.sendMessage(utils.color("&7Player is not online!"));
					sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
					return true;
				}
				if (!args[2].toLowerCase().equals("fragment") && !args[2].toLowerCase().equals("spawnegg") && !args[2].toLowerCase().equals("lootchest")) {
					sender.sendMessage(utils.color("&7Items: SpawnEgg/LootChest/Fragment"));
					sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
					return true;
				}
				if (!args[3].toLowerCase().equals("farming") && !args[3].toLowerCase().equals("fishing") && !args[3].toLowerCase().equals("mining")) {
					sender.sendMessage(utils.color("&7Types: Fishing/Mining/Farming"));
					sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
					return true;
				}
				if (!isInt(args[4])) {
					sender.sendMessage(utils.color("&7Amount is not an integer!"));
					sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
					return true;
				}
				
				for (int i = 0; i < Integer.parseInt(args[4]); i++) {
					Bukkit.getPlayer(args[1]).getInventory().addItem(utils.getItem(args[3], args[2]));
				}
				sender.sendMessage(utils.color("&aSuccessfully gave the item(s) to the player!"));
				return true;
			
			}
			helpMessage(sender);
			return true;
		}
		
		return true;
	}
	
	private void helpMessage(CommandSender sender) {
		sender.sendMessage(utils.color("&8&m-----------------------"));
		sender.sendMessage(utils.color("&c/bf reload"));
		sender.sendMessage(utils.color("&c/bf give <player> <item> <type> <amount>"));
		sender.sendMessage(utils.color("&7Items: SpawnEgg/LootChest/Fragment"));
		sender.sendMessage(utils.color("&7Types: Fishing/Mining/Farming"));
		sender.sendMessage(utils.color("&8&m-----------------------"));
	}
	
	private boolean isInt(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

}
