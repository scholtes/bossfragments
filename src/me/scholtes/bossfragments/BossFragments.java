package me.scholtes.bossfragments;

import org.bukkit.plugin.java.JavaPlugin;

import me.scholtes.bossfragments.listeners.BlockBreak;
import me.scholtes.bossfragments.listeners.BlockPlace;
import me.scholtes.bossfragments.listeners.CraftingEvent;
import me.scholtes.bossfragments.listeners.Fishing;

public class BossFragments extends JavaPlugin {
	
	Utils utils = new Utils(this);
	
	public void onEnable() {
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		getServer().addRecipe(utils.getBossRecipe("fishing"));
		getServer().addRecipe(utils.getBossRecipe("farming"));
		getServer().addRecipe(utils.getBossRecipe("mining"));
		
		getServer().getPluginManager().registerEvents(new CraftingEvent(this, utils), this);
		getServer().getPluginManager().registerEvents(new BlockPlace(this, utils), this);
		getServer().getPluginManager().registerEvents(new Fishing(this, utils), this);
		getServer().getPluginManager().registerEvents(new BlockBreak(this, utils), this);
		
		getCommand("bf").setExecutor(new MainCommand(this, utils));
		
	}
	
	public void onDisable() {

		getServer().clearRecipes();
	}

}
